package com.artivisi.training.microservices201904.catalog.controller;

import com.artivisi.training.microservices201904.catalog.dao.ProdukDao;
import com.artivisi.training.microservices201904.catalog.entity.Produk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class ProdukController {

    @Autowired private ProdukDao produkDao;

    @GetMapping("/hostinfo")
    public Map<String, Object> hostInfo(HttpServletRequest request) throws UnknownHostException {
        Map<String, Object> info = new HashMap<>();
        info.put("hostname", InetAddress.getLocalHost().getHostName());
        info.put("address", request.getLocalAddr());
        info.put("port", request.getLocalPort());
        return info;
    }


    @GetMapping("/produk/")
    public Page<Produk> dataProduk(Pageable page) {
        return produkDao.findAll(page);
    }
}
