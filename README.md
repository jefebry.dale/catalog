# Catalog REST API #

Modul Spring yang digunakan :

* Web : embedded Tomcat, Spring MVC
* Postgresql : Driver JDBC untuk connect ke database Postgresql
* FlywayDB : Tools untuk migrasi database [FlywayDB](https://flywaydb.org/documentation/migrations#sql-based-migrations)
* Spring Data JPA : Library untuk mengurangi coding akses database (tidak perlu tulis SQL untuk CRUD)
* Lombok : generator untuk getter/setter

## Setup Database Postgresql ##

1. Create User Database dengan username `catalog`

    ```
    createuser -U postgres -P catalog
    Enter password for new role : <catalog123>
    Enter again : <catalog123>
    Password : <masukkan password superuser komputer> 
    ```

2. Create Database dengan Owner user `catalog`

    ```
    createdb -U postgres -O catalog catalogdb
    Password : <masukkan password superuser komputer>
    ```

3. Login ke PostgreSQL

    ```
    psql -U catalog -P catalogdb
    ```

4. Melihat daftar tabel

    ```
    \d
    ```

5. Melihat skema tabel produk

    ```
    \d produk
    ```

## Request Encrypt ke Config Server ##

[![Postman Encrypt](docs/img/encrypt-config-server-postman.png)](docs/img/encrypt-config-server-postman.png)